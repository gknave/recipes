const fs = require('fs');
const http = require('http');

const port = process.env.PORT;

fs.readFile('test.json', (err, data) => {
	if (err) throw err;
	let recipeList = JSON.parse(data);
	console.log(recipeList);
});

const server = http.createServer((req, res) => {
	//my http server
	res.statusCode = 200;
	res.setHeader('Content-Type', 'text/html');
	res.end('<h1>Hello, World!</h1>');
});

server.listen(port, () => {
	console.log(`Server running at port ${port}`)
})

console.log('Done')
