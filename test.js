var fs = require('fs');

const recipeList = document.getElementById('recipeList');
const searchBar = document.getElementById('searchBar');

let allRecipes = [];


searchBar.addEventListener("keyup", function(e) {
	const searchString = e.target.value.toLowerCase();
	const filteredRecipes = allRecipes.filter( recipe => {
		return (
			recipe.name.toLowerCase().includes(searchString) || 
			recipe.ingredients.toLowerCase().includes(searchString)
		);
	});
	console.log(filteredRecipes);
	displayRecipes(filteredRecipes);
});

const loadRecipes = async () => {
	fs.readFile('./content.json', (err, data) => {
	    if (err)
		console.log(err);
	    else {
		var allRecipes = JSON.parse(data);
		displayRecipes(allRecipes);
		console.log(allRecipes);
	    }
	})
};

const displayRecipes = (recipes) => {
	const htmlString = recipes
		.map((recipe) => {
			return `
			<li class="Recipe">
				<h2>${recipe.name}</h2>
				<p>Cook time: ${recipe.time}</p>
			</li>
		`;
		})
		.join('');
	recipeList.innerHTML = htmlString;
};

loadRecipes();
